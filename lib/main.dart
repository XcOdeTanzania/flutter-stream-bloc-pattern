import 'package:flutter/material.dart';
import 'package:qr_code/blocs/user_bloc.dart';
import 'package:qr_code/pages/bloc_user_page.dart';
import 'package:qr_code/widget/bloc_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Streams',
      theme: ThemeData.light(),
      home:  BlocProvider(
        bloc: UserBloc(),
        child: BlocUserPage(),
      ),
    );
  }
}
