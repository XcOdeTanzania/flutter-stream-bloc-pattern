import 'package:flutter/material.dart';
import 'package:qr_code/blocs/user_bloc.dart';
import 'package:qr_code/models/user_model.dart';
import 'package:qr_code/widget/bloc_provider.dart';

class BlocUserPage extends StatefulWidget {
  @override
  _BlocUserPageState createState() => _BlocUserPageState();
}

class _BlocUserPageState extends State<BlocUserPage> {
  String _userName;
  @override
  Widget build(BuildContext context) {
    final UserBloc userBloc = BlocProvider.of<UserBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Bloc User"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder<User>(
              //we are litsening to the stream
              //when data comes of of the stream we update the stream
              stream: userBloc.outUser,
              initialData: User.empty(),
              builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
                return Text(
                  '${snapshot.data.name}',
                  style: Theme.of(context).textTheme.body1,
                );
              },
            ),
            StreamBuilder<User>(
              //we are litsening to the stream
              //when data comes of of the stream we update the stream
              stream: userBloc.outUser,
              initialData: User.empty(),
              builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
                return TextField(
                  onChanged: (value) => _userName = value,
                  decoration: InputDecoration(labelText: 'Name'),
                  onSubmitted:(v)=> userBloc.updateName.add(_userName),
                );
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //counterBloc.incrementCounter.add(null);
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
