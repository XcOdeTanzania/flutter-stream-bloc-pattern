import 'dart:async';

import 'package:flutter/material.dart';

class StreamCounterPage extends StatefulWidget {
  @override
  _StreamCounterPageState createState() => _StreamCounterPageState();
}

class _StreamCounterPageState extends State<StreamCounterPage> {

  final StreamController<int> _streamController = StreamController<int>();

  int _counter = 0;

  @override
  void dispose(){
    _streamController.close();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
appBar: AppBar(
  title: Text("Stream"),
),
body: Center(
  child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text("You have pushed the button this number of times"),
     StreamBuilder<int>(
       //we are litsening to the stream
       //when data comes of of the stream we update the stream
       stream: _streamController.stream,
       initialData: 0,
       builder: (BuildContext context, AsyncSnapshot<int> snapshot){
         return  Text('${snapshot.data}', style: Theme.of(context).textTheme.body1,);
       },

     )
    ],
  ),
),
floatingActionButton: FloatingActionButton(
  onPressed: (){
    _streamController.sink.add(++_counter);
  },
  tooltip: 'Increment',
  child: const Icon(Icons.add),
),
    );
  }
}